
function __G__TRACKBACK__(errorMessage)
    print("----------------------------------------")
    print("LUA ERROR: " .. tostring(errorMessage) .. "\n")
    print(debug.traceback("", 2))
    print("----------------------------------------")
end

package.path = package.path .. ";src/"
cc.FileUtils:getInstance():setPopupNotify(false)
require("app.MyApp").new():run()

--[[
	main 文件的最后一行代码中通过载入的 app.MyApp 模块创建了一个 MyApp 实例，
	并且还调用执行了该 MyApp 实例的 run 方法。这行代码将启动并执行 MyApp 脚本。
	从此处我们也可以看出，main.lua 文件是应用程序 lua 脚本的启动文件。

]]