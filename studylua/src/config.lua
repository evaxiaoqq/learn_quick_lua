
-- 0 - disable debug info, 1 - less debug info, 2 - verbose debug info
DEBUG = 1

-- display FPS stats on screen
DEBUG_FPS = true

-- dump memory info every 10 seconds
--是否要每10秒打印一次内存信息
DEBUG_MEM = false

-- load deprecated API
--是否加载已经废弃了的API
LOAD_DEPRECATED_API = false

-- load shortcodes API
-- 是否加载短代码
LOAD_SHORTCODES_API = true

-- design resolution
CONFIG_SCREEN_WIDTH  = 1136 --屏幕宽度，但屏幕方向为“landscape”横屏时，该属性表示屏幕的高度
CONFIG_SCREEN_HEIGHT = 960 --屏幕高度，但屏幕方向为“landscape”横屏时，该属性表示屏幕的宽度


--游戏对象标签
GROUND_TAG = 1
HEART_TAG = 2
BIRD_TAG = 3
AIRSHIP_TAG= 4
PLAYER_TAG = 5

-- auto scale mode
--屏幕适配策略，如FIXED_WIDTH、FIXED_HEIGHT和FILL_ALL
CONFIG_SCREEN_AUTOSCALE = "FIXED_WIDTH"


