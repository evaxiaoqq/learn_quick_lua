
local Heart = require("app.objects.Heart")
local Airship = require("app.objects.Airship")
local Bird = require("app.objects.Bird")


BackgroundLayer = class("BackgroundLayer", function()
	return display.newLayer()
end)
function BackgroundLayer:ctor()
	self.distanceBg = {}
	self.nearbyBg = {}
	self.tiledMapBg={}

	--添加飞鸟
	self.birds={}

	
	self:createBackgrounds()

	--添加物理边界
	local width = self.map:getContentSize().width
	local height1 = self.map:getContentSize().height*9/10
	local height2 = self.map:getContentSize().height*3/16

	local sky = display.newNode()
	local bodyTop = cc.PhysicsBody:createEdgeSegment(cc.p(0,height1),cc.p(width,height1))
	--设置碰撞系数
	bodyTop:setCategoryBitmask(0x1000)
	bodyTop:setContactTestBitmask(0x0000)
	bodyTop:setCollisionBitmask(0x0001)

	sky:setPhysicsBody(bodyTop)

	self:addChild(sky)

	local ground = display.newNode()
	local bodyBottom = cc.PhysicsBody:createEdgeSegment(cc.p(0, height2), cc.p(width, height2))
	--设置碰撞系数
	bodyBottom:setCategoryBitmask(0x1000)
	bodyBottom:setContactTestBitmask(0x0001)
	bodyBottom:setCollisionBitmask(0x0011)
	ground:setTag(GROUND_TAG)

	ground:setPhysicsBody(bodyBottom)
	self:addChild(ground)
	--[[
		createEdgeSegment 方法能创建一个不受重力约束的自由线条，它有四个参数，分别表示：

		参数1为 cc.p 类型，表示线条的起点；
		参数2也为 cc.p 类型，表示线条的终点；
		参数3为 cc.PhysicsMaterial 类型，表示物理材质的属性，默认情况下为 cc.PHYSICSBODY_MATERIAL_DEFAULT；
		参数4为 number 类型，表示线条粗细。

	]]
	self:startGame()

	self:addBody("heart", Heart)
	self:addBody("airship", Airship)
	self:addBody("bird", Bird)

end
function BackgroundLayer:startGame()
	--添加帧事件 帧事件在游戏中经常用来更新游戏中的数据
	print("startgame")
	self:addNodeEventListener(cc.NODE_ENTER_FRAME_EVENT, handler(self,self.scrollBackgrounds))
    self:scheduleUpdate()
    --[[
		addNodeEventListener 方法用于注册帧事件，scheduleUpdate 方法则启用了帧事件，
		只有调用了 scheduleUpdate 后，帧事件才会被触发。
    ]]
end
function BackgroundLayer:createBackgrounds()
	print("createBackgrounds")
	--创建布景
	-- local bg= display.newSprite("image/b1.png")
	-- 	:pos(display.cx, display.cy)
	-- 	:addTo(self,-4)
	-- 创建布幕背景
	local bg = display.newSprite("image/bj2.jpg")
		:pos(display.cx, display.cy)
		:addTo(self, -4)
	--addTo方法中可以指定游戏元素的 z-order 值，本教程中，我们把布幕背景的z-order设置为-4，确保它位于场景的最下层

	local bg1 = display.newSprite("image/b2.png")
		:align(display.BOTTOM_LEFT, display.left,display.bottom+10)
		:addTo(self,-3)
	local bg2 = display.newSprite("image/b2.png")
		:align(display.BOTTOM_LEFT, display.left+bg1:getContentSize().width,display.bottom+10)
		:addTo(self,-3)
	--背景1 2 添加到背景table中
	table.insert(self.distanceBg,bg1)
	table.insert(self.distanceBg,bg2)

	--添加地图文件
	self.map = cc.TMXTiledMap:create("image/map.tmx")
		:align(display.BOTTOM_LEFT, display.left, display.bottom)
		:addTo(self,-1)

	


end
--实现滚动背景，需要做的就是不断的改变背景图片贴图横坐标，并且不断的刷新位置
function BackgroundLayer:scrollBackgrounds(dt)
	if self.distanceBg[2]:getPositionX()<=0 then
		self.distanceBg[1]:setPositionX(0)
	end
 
    --50*dt 相当于速度
	local x1 = self.distanceBg[1]:getPositionX()-50*dt
	local x2 = x1 + self.distanceBg[1]:getContentSize().width
	--设置背景图位置
	self.distanceBg[1]:setPositionX(x1)
	self.distanceBg[2]:setPositionX(x2)
	--滚动地图文件
	if self.map:getPositionX() <= display.width - self.map:getContentSize().width then
		self:unscheduleUpdate()
	end
    local x5=self.map:getPositionX() - 130*dt 
	self.map:setPositionX(x5)

	--刷新飞鸟速度
	self:addVelocityToBird()
end
function BackgroundLayer:addBody(objectGroupName,class)
	--addHeart方法的作用是遍历 TiledMap 中的 对象层，
	--取得该对象层中所有对象的坐标，并在该坐标上创建一个个心心对象
	local objects = self.map:getObjectGroup(objectGroupName):getObjects()
	local dict=nil
	local i = 0
	local len = #objects
	for i=0,len-1,1 do
		dict = objects[i+1]
		if dict == nil then
			break
		end
		local x=dict["x"]
		local y=dict["y"]

		local sprite = class.new(x,y)
		self.map:addChild(sprite)
		if objectGroupName == "bird" then
			table.insert(self.birds,sprite)
		end
	end
end
--给飞鸟添加加速度
function BackgroundLayer:addVelocityToBird()
	local dict=nil
	local i = 0
	local len = #self.birds
	for i=0,len-1,1 do
		dict = self.birds[i+1]
		if dict == nil then
			break
		end
		local x = dict:getPositionX()
		if x <= display.width - self.map:getPositionX() then
			if dict:getPhysicsBody():getVelocity().x==0 then
				dict:getPhysicsBody():setVelocity(cc.p(-70,math.random(-40,40)))
			else
				table.remove(self.birds,i+1)
			end
		end
	end
end
return BackgroundLayer