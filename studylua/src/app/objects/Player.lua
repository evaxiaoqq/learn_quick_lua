
Player = class("Player", function()
	return display.newSprite("#flying1.png")
end)	

function Player:ctor()
	--引擎封装了 Physics 接口后，Node 就自带了 body 属性，
	--也就是每个 Sprite 都自带了 body 属性

	local body = cc.PhysicsBody:createBox(self:getContentSize(),cc.PHYSICSBODY_MATERIAL_DEFAULT,cc.p(0,0))
	
	--[[
		setRotationEnable(enable)：如果你想你的刚体不旋转，那么你可以调用该函数。
		如 Player、Airship 和 Bird 对象，它们在物理世界中是 Dynamic（动态）的，
		但我们不希望它们在动的过程中重心不稳发生旋转，所以，我们可以给 Player 刚体添加
		这样的属性加以约束，即：body:setRotationEnable(false)。
	]]
	body:setRotationEnable(false)
	--设置碰撞系数
	body:setCategoryBitmask(0x0111)
	body:setContactTestBitmask(0x1111)
	body:setCollisionBitmask(0x1001)
	self:setTag(PLAYER_TAG)


	--PHYSICSBODY_MATERIAL_DEFAULT宏表示的是创建的Body的默认材质
	self:setPhysicsBody(body)
	self:getPhysicsBody():setGravityEnable(false) 
	--加载动作缓存
	self:addAnimationCache()

	--[[
		cc.PhysicsBody:createBox()方法创建了一个矩形的 body，createBox 方法有三个参数，分别是：
		参数1为 cc.size 类型，它表示矩形 body 的尺寸大小。
		参数2为 cc.PhysicsMaterial 类型，表示物理材质的属性，默认情况下为 cc.PHYSICSBODY_MATERIAL_DEFAULT。 
		    该参数也可自定义，方法如下：
		    cc.PhysicsMaterial(density, restitution, friction)
			density：表示密度
			restitution：表示反弹力
			friction：表示摩擦力
		参数3为 cc.p 类型，它也是一个可选参数，表示 body 与中心点的偏移量，默认下为cc.p(0,0)
			与 createBox 方法类似的还有 createCircle(radius, material, offset)，该方法可以创建一个圆形的 body，除第一个参数为半径外，其余两参数与 createBox 方法一样
	]]


end
--添加动作动画到缓存中
function Player:addAnimationCache()
	local animatons = {"flying","drop","die"}
	local animationFrameNum = {4,2,4}
	for i = 1,#animatons do
		local frames = display.newFrames(animatons[i].."%d.png",1,animationFrameNum[i])
		local animation = display.newAnimation(frames,0.3/4)
		display.setAnimationCache(animatons[i],animation)
	end
end

function Player:flying()
   transition.stopTarget(self)
   transition.playAnimationForever(self, display.getAnimationCache("flying"))
end
function Player:drop()
   transition.stopTarget(self)
   transition.playAnimationForever(self, display.getAnimationCache("drop"))
end
function Player:die()
   transition.stopTarget(self)
   transition.playAnimationOnce(self, display.getAnimationCache("die"))
end
function Player:hit()
    local hit = display.newSprite()
    hit:setPosition(self:getContentSize().width / 2, self:getContentSize().height / 2)
    self:addChild(hit)
    local frames = display.newFrames("attack%d.png", 1, 6)
    local animation = display.newAnimation(frames, 0.3 / 6)
    local animate = cc.Animate:create(animation)

    local sequence = transition.sequence({
        animate,
        cc.CallFunc:create(function()
            hit:removeSelf()
        end)
    })

    hit:runAction(sequence)
    hit:setScale(0.6)
    
end
--添加血条
function Player:createProgress()
	self.blood = 100
	--进度条背景
	local progressbg = display.newSprite("#progress1.png")
	--添加进度条组件
	--创建进度条 self.fill，display.newProgressTimer 方法的两个参数分别表示创建进度条所用的图片，
	--以及进度条的类型。引擎中进度条的类型有两种：
	--display.PROGRESS_TIMER_BAR：条形，这种类型的进度条通常用于创建类似生命条，等级条的一类进度条。
	--display.PROGRESS_TIMER_RADIAL：环形，它通常用于创建类似加载功能的进度条，
	--比如游戏加载资源时页面上旋转的菊花（PS：词穷，就是菊花吧，不要想歪）。
	self.fill = display.newProgressTimer("#progress2.png",display.PROGRESS_TIMER_BAR)
	--设置进度条的起始点，（0，y）表示最左边，（1，y）表示最右边，（x，1）表示最上面，（x，0）表示最下面。
	self.fill:setMidpoint(cc.p(0,0.5))
	--设置进度条变化方向，如果不用变化的方向，则设置该方向为0，否则设置为1。所以（1,0）表示横方向，（0,1）表示纵方向。
	self.fill:setBarChangeRate(cc.p(1.0,0))
	self.fill:setPosition(progressbg:getContentSize().width/2, progressbg:getContentSize().height/2)
	progressbg:addChild(self.fill)
	--调用 ProgressTimer 的 setPercentage 方法设置进度条的百分比为 100%（因为 self.blood 的值为100）
	self.fill:setPercentage(self.blood)

	progressbg:setAnchorPoint(cc.p(0,1))
	self:getParent():addChild(progressbg)
	progressbg:setPosition(cc.p(display.left,display.top))

end
function Player:setProPercentage(Percentage)
	self.fill:setPercentage(Percentage)
end
return Player
