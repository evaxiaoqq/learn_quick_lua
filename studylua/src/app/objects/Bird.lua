--创建障碍物－飞艇
local Bird = class("Bird", function()
	return display.newSprite("#bird1.png")
end)

local MATERIAL_DEFAULT = cc.PhysicsMaterial(0.0, 0.0, 0.0) 
function Bird:ctor(x,y)
	local birdSize = self:getContentSize()

	local BirdBody = cc.PhysicsBody:createCircle(birdSize.width/2,MATERIAL_DEFAULT)
	--[[
		setRotationEnable(enable)：如果你想你的刚体不旋转，那么你可以调用该函数。
		如 Player、Airship 和 Bird 对象，它们在物理世界中是 Dynamic（动态）的，
		但我们不希望它们在动的过程中重心不稳发生旋转，所以，我们可以给 Player 刚体添加
		这样的属性加以约束，即：body:setRotationEnable(false)。
	]]
	BirdBody:setRotationEnable(false)
	--设置碰撞系数
	BirdBody:setCategoryBitmask(0x0100)
	BirdBody:setContactTestBitmask(0x0100)
	BirdBody:setCollisionBitmask(0x1000)
	self:setTag(BIRD_TAG)

	
	self:setPhysicsBody(BirdBody)
	self:getPhysicsBody():setGravityEnable(false)

	self:setPosition(x,y)

	local frames = display.newFrames("bird%d.png",1,9)
	local animation = display.newAnimation(frames,0.5/9)
	animation:setDelayPerUnit(0.1)

	local animate = cc.Animate:create(animation)
	self:runAction(cc.RepeatForever:create(animate))
end
return Bird