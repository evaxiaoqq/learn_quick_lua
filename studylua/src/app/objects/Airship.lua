--创建障碍物－飞艇
local Airship = class("Airship", function()
	return display.newSprite("#airship.png")
end)

local MATERIAL_DEFAULT = cc.PhysicsMaterial(0.0, 0.0, 0.0) 
function Airship:ctor(x,y)
	local airshopSize = self:getContentSize()

	local airshipBody = cc.PhysicsBody:createCircle(airshopSize.width/2,MATERIAL_DEFAULT)
	--[[
		setRotationEnable(enable)：如果你想你的刚体不旋转，那么你可以调用该函数。
		如 Player、Airship 和 Bird 对象，它们在物理世界中是 Dynamic（动态）的，
		但我们不希望它们在动的过程中重心不稳发生旋转，所以，我们可以给 Player 刚体添加
		这样的属性加以约束，即：body:setRotationEnable(false)。
	]]
	airshipBody:setRotationEnable(false)
	--设置碰撞系数
	--设置碰撞系数
	airshipBody:setCategoryBitmask(0x0100)
	airshipBody:setContactTestBitmask(0x0100)
	airshipBody:setCollisionBitmask(0x1000)
	self:setTag(AIRSHIP_TAG)



	self:setPhysicsBody(airshipBody)
	self:getPhysicsBody():setGravityEnable(false)

	self:setPosition(x,y)

	local move1 = cc.MoveBy:create(3,cc.p(0,airshopSize.height/2))
	local move2 = cc.MoveBy:create(3,cc.p(0,-airshopSize.height/2))
	local SequenceAction = cc.Sequence:create(move1,move2)
	transition.execute(self,cc.RepeatForever:create(SequenceAction))
end
return Airship