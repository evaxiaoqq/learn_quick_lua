local Heart=class("Heart", function()
	return display.newSprite("image/heart.png")
end)
--设置物理特性
--这里我们在创建刚体时把它的密度，反弹力、摩擦力都设为0是为了在碰撞的时候不发生任何物理形变。
local MATERIAL_DEFAULT = cc.PhysicsMaterial(0.0,0.0,0.0)

function Heart:ctor(x,y)
	local heartBody=cc.PhysicsBody:createCircle(self:getContentSize().width/2,
					MATERIAL_DEFAULT)

	--[[
		setDynamic(dynamic)：如果你想你的刚体固定不动，那么你可以调用该函数。
		如游戏中的心心，它在物理世界中就是个相对固定不动的对象，所以我们可以设置它
		的刚体属性dynamic为false，即 body:setDynamic(false) 。
	]]
	heartBody:setDynamic(false)
	--设置碰撞系数
	heartBody:setCategoryBitmask(0x0001)
	heartBody:setContactTestBitmask(0x0100)
	heartBody:setCollisionBitmask(0x0001)
	self:setTag(HEART_TAG)

	self:setPhysicsBody(heartBody)

	self:getPhysicsBody():setGravityEnable(false)

	self:setPosition(x, y)
end

return Heart