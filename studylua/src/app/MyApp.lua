--1、使用 require 方法载入配置、cocos 以及 Quick 框架
require("config")
require("cocos.init")
require("framework.init")--quick lua 框架
require("app.layers.BackgroundLayer")
require("app.objects.Player")
--2、使用 class 函数定义一个 MyApp 类。 
local MyApp = class("MyApp", cc.mvc.AppBase)

function MyApp:ctor()
	--ctor()函数相当于 C++ 中的构造（constructor）函数，以及init()函数。
	--所以我们一般会在ctor()函数中初始化我们的游戏数据和信息，每当调用 XXClass.new()
	--创建对象实例时，也会自动执行ctor()函数。 
    MyApp.super.ctor(self)
    --在子类的ctor函数中必须手动调用父类的构造函数，这样才等保证子类能“继承”父类的属性和方法
    --MyApp获取父类AppBase的属性和方法
end

function MyApp:run()
    --设置随机种子
    math.randomseed(os.time())
	--addSearchPath 方法设置资源搜索路径
    cc.FileUtils:getInstance():addSearchPath("res/")
    --添加资源缓存文件
    display.addSpriteFrames("image/player.plist","image/player.pvr.ccz")
    audio.preloadMusic("sound/background.mp3") 
    audio.preloadSound("sound/button.wav")
    audio.preloadSound("sound/ground.mp3")
    audio.preloadSound("sound/heart.mp3")
    audio.preloadSound("sound/hit.mp3")
    --调用 enterScene 函数进入到名为 “MainScene” 的游戏场景。
    self:enterScene("MainScene")
end

return MyApp
