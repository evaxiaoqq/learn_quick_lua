local Player = require("app.objects.Player")

--[[
	载入文件到 Quick 项目可以使用以下的两种方式：
	1.通过require()方法，该方法会搜索指定的目录，并加载文件。
	2.通过import()方法，该方法用于处理require同目录下其他模块，在模块名前加.。
]]
local GameScene=class("GameScene",function()
	--创建物理引擎场景
	return display.newPhysicsScene("GameScene")
end)
function GameScene:ctor()
    print("gamescene ctor")
    self.world = self:getPhysicsWorld()
    self.world:setGravity(cc.p(0,-98.0))
    --self.world:setDebugDrawMask(cc.PhysicsWorld.DEBUGDRAW_ALL)
    --[[
		self:getPhysicsWorld()用来获取场景绑定的物理世界对象。 
		获取的 self.world 默认是有带重力的，大小为(0.0f, -98.0f), 但我们也可以通过的
		setGravity()方法来改变重力大小。 
		setDebugDrawMask 方法是在调试物理世界时使用的，
		该方法可以开启调试模，能把物理世界中不可见的body，shape，joint
		等元素可视化（如下图所示）。当调试结束时，需要把该功能关闭。
    ]]
	self.backgroundLayer=BackgroundLayer.new()
		 :addTo(self)

	self.player = Player.new()
		:addTo(self)
	self.player:setPosition(-20, display.height*2/3)
	self:playerFlyToScene()

	--添加进度条
	self.player:createProgress()

	--添加碰撞检测
	self:addCollision()
end
function GameScene:playerFlyToScene()
	local function startDrop()
		self.player:getPhysicsBody():setGravityEnable(true)
		self.player:drop()
		--开始游戏
		self.backgroundLayer:startGame()
		--开启触摸控制
		self.backgroundLayer:addNodeEventListener(cc.NODE_TOUCH_EVENT, function(event)
				return self:onTouch(event.name,event.x,event.y)
			end)
		self.backgroundLayer:setTouchEnabled(true)
	end

	local animation = display.getAnimationCache("flying")
	transition.playAnimationForever(self.player, animation)

	local action =  transition.sequence({
			cc.MoveTo:create(4,cc.p(display.cx,display.height*2/3)),
			cc.CallFunc:create(startDrop)
		})

	self.player:runAction(action)

end
--onTouch事件
function GameScene:onTouch(event,x,y)
	print("event = "..event.."(x="..x..",y="..y..")")
	if event == "began" then
		self.player:flying()
		--为主角添加向上的速度
		self.player:getPhysicsBody():setVelocity(cc.p(0,98))
		return true
	--elseif event == "moved" then
	elseif event == "ended" then
		self.player:drop()
	end
end


function GameScene:onEnter()

end
function GameScene:onExit()
	
end
function GameScene:addCollision()
	local function contactLogic(node)
		if node:getTag() == HEART_TAG then
			-- 给玩家增加血量，并添加心心消除特效，
			print("给玩家增加血量，并添加心心消除特效")
			--添加心心消除特效
			local emitter = cc.ParticleSystemQuad:create("particles/stars.plist")
			emitter:setBlendAdditive(false)
			emitter:setPosition(node:getPosition())
			self.backgroundLayer.map:addChild(emitter)
			if self.player.blood < 100 then
				self.player.blood = self.player.blood + 2
				self.player:setProPercentage(self.player.blood)
			end
			--播放音效
			audio.playSound("sound/heart.mp3")

			node:removeFromParent()
		elseif node:getTag() == GROUND_TAG then
			-- 减少玩家20点血量，并添加玩家受伤动画
			self.player:hit()
			self.player.blood = self.player.blood - 20
            self.player:setProPercentage(self.player.blood)
            audio.playSound("sound/ground.mp3")

			print("减少玩家20点血量，并添加玩家受伤动画")
		elseif node:getTag() == AIRSHIP_TAG then
			-- 减少玩家10点血量，并添加玩家受伤动画
			self.player:hit()
			self.player.blood = self.player.blood - 10
            self.player:setProPercentage(self.player.blood)
            audio.playSound("sound/hit.mp3")
			print("减少玩家10点血量，并添加玩家受伤动画")
		elseif node:getTag() == BIRD_TAG then
			-- 减少玩家5点血量，并添加玩家受伤动画
			print("减少玩家5点血量，并添加玩家受伤动画")
			self.player:hit()
			self.player.blood = self.player.blood - 5
            self.player:setProPercentage(self.player.blood)
            audio.playSound("sound/hit.mp3")
		end
	end
	local function onContactBegin(contact)
		local a = contact:getShapeA():getBody():getNode()
		local b = contact:getShapeB():getBody():getNode()
		contactLogic(a)
		contactLogic(b)
		return true
	end
	local function onContactSeperate(contact)
		---- 在这里检测当玩家的血量减少是否为0，游戏是否结束。
		print("检测当玩家的血量减少是否为0")
		 if self.player.blood <= 0 then 
            self.backgroundLayer:unscheduleUpdate()
            self.player:die()

            local over = display.newSprite("image/over.png")
                :pos(display.cx, display.cy)
                :addTo(self)
            --移除所有事件
            cc.Director:getInstance():getEventDispatcher():removeAllEventListeners()
        end
	end
	local contactListener = cc.EventListenerPhysicsContact:create()
    contactListener:registerScriptHandler(onContactBegin, cc.Handler.EVENT_PHYSICS_CONTACT_BEGIN)
    contactListener:registerScriptHandler(onContactSeperate, cc.Handler.EVENT_PHYSICS_CONTACT_SEPERATE)
    local eventDispatcher = cc.Director:getInstance():getEventDispatcher()
    eventDispatcher:addEventListenerWithFixedPriority(contactListener, 1)


end
--[[碰撞检测分析
   下面我们来看看添加碰撞监听的代码，如下所示：

	-- 1
	local contactListener = cc.EventListenerPhysicsContact:create()
	-- 2
	contactListener:registerScriptHandler(onContactBegin, cc.Handler.EVENT_PHYSICS_CONTACT_BEGIN)   
	-- 3
	contactListener:registerScriptHandler(onContactSeperate, cc.Handler.EVENT_PHYSICS_CONTACT_SEPERATE)   
	-- 4
	local eventDispatcher = cc.Director:getInstance():getEventDispatcher()   
	-- 5
	eventDispatcher:addEventListenerWithFixedPriority(contactListener, 1)
	创建物体碰撞检测事件监听器对象（contactListener）；
	设置监听器的碰撞开始函数 onContactBegin；
	设置监听器的碰撞分离函数 onContactSeperate；
	监听器设置完毕，需要把它加入到引擎导演的事件分发器中。所以这里获取游戏的事件分发器（eventDispatcher）；
	将检测事件监听器（contactListener）添加到事件分发器（eventDispatcher）中，这样就可以触发碰撞检测事件。
	addEventListenerWithFixedPriority 方法是指定固定的事件优先级注册监听器，事件优先级决定事件响应的优先级别，值越小优先级越高。


	掩码属性

	在讲解 onContactBegin 和 onContactSeperate 函数之前，这里我们需要清楚地一点。即在默认情况下，
	物理引擎中的物体是不会触发碰撞回调事件的。也就是说，上面代码中的 onContactBegin 和 onContactSeperate 方法永远都不会调用到。

	为什么啦？ O(∩_∩)O~呵呵，咱不卖关子。因为每个 cc.PhysicsBody 都具有三个掩码属性，两个刚体能不能碰撞，能不能发送接触事件信息，
	都依靠于这三个参数的值。

	所以，为了更好地解决刚才遇到的问题，下面我们先来了解下刚体的这三个 mask 属性。

	CategoryBitmask：32位整型，刚体的类别掩码。它定义了一个物体所属类别，每一个物体在场景中都能被分配到多达32位不同的类别。
					默认值为0xFFFFFFFF。

	ContactTestBitmask：32位整型，刚体的接触测试掩码。当两个物体接触时，用一个物体的类别掩码与另一个物体的接触测试掩码做“逻辑与”运行，
						如果结果为非零值，引擎才会新建 PhysicsContact 对象，发送碰撞事件。那么才发送碰撞事件消息。 
	ContactTestBitmask 的设计是为了优化性能，并不是所有物体之间的碰撞我们都关心，所有这个 ContactTestBitmask 的
						默认值为0x00000000。

	CollisionBitmask：32位整型，刚体的碰撞掩码。当两个物体接触后，用一个物体的碰撞掩码与另一个物体的类别掩码执行“逻辑与”运算，
						如果结果为非零值，那么该物体能够对另一个物体的碰撞发生反应。这里的“碰撞反应”会表现为一个物体受到另外物体的碰撞，
						而改变运动方向。默认值为0xFFFFFFFF。
	总结：

	CategoryBitmask 是其它两个掩码比较的基础。
	CategoryBitmask & ContactTestBitmask 决定是否发送事件消息。
	CategoryBitmask & CollisionBitmask 决定是否产生刚体反弹效果。
	ContactTestBitmask 和 CollisionBitmask 互相之间没有联系。

	注：每个 mask 都有对应的 get 和 set 接口来获取或者修改mask。 
		另外，发生碰撞和发送事件消息是不同的概念，前者是直观地一种表现-碰撞反弹，后者是一种消息机制，就是说是否调用碰撞事件的回调函数。

	节点			类别掩码	  接触测试掩码    碰撞掩码
	玩家:Player	0111		1111		1001
	心心:Heart	0001		0100		0001
	鸟: Bird		0010		0010		1000
	飞艇 		0100		0100		1000
	地面			1000		0001		0011
	天界			1000		0000		0001
]]
return GameScene