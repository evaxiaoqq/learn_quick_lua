-- class 函数创建了一个 MainScene 场景类 通过函数创建一个场景对象
local MainScene = class("MainScene", function()
    return display.newScene("MainScene")
    --display.newScene() 方法将创建了一个新场景，并返回该 Scene 场景对象
end)
--[[
	display 模块封装了绝大部分与显示有关的功能，
	并负责根据 config.lua 中定义的分辨率设定计算
	屏幕的设计分辨率。display 模块提供了众多的方法
	和属性，比如创建层（display.newLayer），创建精灵
	（display.newSprite），以及恢复暂停切换场景等等
]]
--[[
	isplay.widthInPixels, display.heightInPixels： 屏幕分辨率的宽、高
	display.width, display.height： 设计分辨率的宽、高
	display.cx, display.cy： 设计分辨率中央的 x、y 坐标
	display.left, display.top, display.right, display.bottom： 设计分辨率四边的坐标
	display.c_left, display.c_top, display.c_right, display.c_bottom： 当父对象在屏幕中央时，屏幕四边的坐标
	display.contentScaleFactor： 设计分辨率到屏幕分辨率的缩放因子，不同于内容缩放因子。
]]
function MainScene:ctor()
	--[[
		cc.ui 模块中封装了大量符合脚本风格的 Cocos2d-x 控件，
		包含 UILabel、UIImage、UISlider 等等。所以正如上述
		代码一样，我们可以通过调用cc.ui.UILabel.new()来实例化
		一个新的 UILabel 控件。
	]]
	-- cc.ui.UILabel.new({UILabelType=2,text="hello lua",size=64})
	-- 				  :align(display.CENTER, display.cx, display.cy)
	-- 				  :addTo(self)
    -- cc.ui.UILabel.new({
    --         UILabelType = 2, text = "Hello, World", size = 64})
    --     :align(display.CENTER, display.cx, display.cy)
    --     :addTo(self)
    --[[
		UILabelType：创建文本对象所使用的方式。 
		1 表示使用位图字体创建文本显示对象，返回对象是 LabelBMFont。 
		2 表示使用 TTF 字体创建文字显示对象，返回对象是 Label。
		text：要显示的文本。
		size：文字尺寸。
		创建好 UILabel 后，通过调用 align 方法设置文本的锚点和显示位置。最后用 addTo 方法把文本添加到场景中
    ]]

    --添加精灵
    display.newSprite("image/main.jpg")
    		:pos(display.cx,display.cy)
    		:addTo(self)
   	--添加标题
    local title = display.newSprite("image/title.png")
    title:pos(display.cx,display.height*2/3)
    title:addTo(self)
    --动画
    local move1=cc.MoveBy:create(0.5,cc.p(0,10))
    local move2=cc.MoveBy:create(0.5,cc.p(0,-10))
    local SequenceAction=cc.Sequence:create(move1,move2)
    transition.execute(title,cc.RepeatForever:create(SequenceAction))
    --[[
		其中 transition.execute(target, action, args) 方法用于执行一个动作效果，它可以为原本单一的
		动作添加各种附加特性。其参数分别代表：
		target： 显示对象(cc.Node)
		action： 动作对象
		args： table 参数表格对象
    ]]
    --添加按钮
    cc.ui.UIPushButton.new({normal="image/start1.png",pressed="image/start2.png"})
    						:onButtonClicked(function()
    							--print("start click")
    							app:enterScene("GameScene", nil, "SLIDEINT", 1.0)
    						end)	
    						:pos(display.cx,display.cy)
    						:addTo(self)

    --[[
		onButtonPressed(callback)：用于监听按钮的按下事件
		onButtonRelease(callback)：用于监听按钮的释放事件
		onButtonStateChanged(callback)：用于监听按钮的状态改变事件
    ]]




    
    
end
--进入游戏场景调用
function MainScene:onEnter()
end
--离开游戏场景调用
function MainScene:onExit()
end

return MainScene
